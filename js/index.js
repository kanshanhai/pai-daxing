window.addEventListener("load", res => {
	function eyeball() {
		const eyes = document.querySelectorAll(".eye");
		eyes.forEach(res => {
			let x = res.getBoundingClientRect().left + res.clientWidth / 2;
			let y = res.getBoundingClientRect().top + res.clientHeight / 2;

			const event = window.event;

			let radian = Math.atan2(event.pageY - y, event.pageX - x);
			let rotation = radian * (180 / Math.PI) + 180;

			res.style.transform = `rotate(${rotation}deg)`;
		})
	}
	document.querySelector("body").addEventListener("mousemove", eyeball);

	// 获取拖拽盒子的元素对象
	var dragBox = document.getElementById('myDragBox');
	var hashDiv = document.getElementsByClassName("hashDiv");
	// 初始位置和鼠标位置的差值
	var offsetX, offsetY;
	// 拖动状态
	var isDragging = false;
	// 鼠标按下事件监听器
	dragBox.addEventListener('mousedown', function(e) {
		isDragging = true;
		offsetX = e.clientX - dragBox.offsetLeft;
		offsetY = e.clientY - dragBox.offsetTop;
		dragBox.style.background = "url('img/pdx5.jpg')";
		dragBox.style.backgroundSize = "100% 100%"
		hashDiv[0].style.display = "none"
	});
	// 鼠标移动事件监听器
	document.addEventListener('mousemove', function(e) {
		if (isDragging) {
			dragBox.style.left = e.clientX - offsetX + 'px';
			dragBox.style.top = e.clientY - offsetY + 'px';
			if (e.clientX - offsetX < 700) {
				dragBox.style.background = "url('img/pdx6.jpg')";
			} else {
				dragBox.style.background = "url('img/pdx5.jpg')";
			}
		}
		dragBox.style.backgroundSize = "100% 100%"
	});
	// 鼠标释放事件监听器
	document.addEventListener('mouseup', function(e) {
		isDragging = false;
		hashDiv[0].style.display = ""
		dragBox.style.background = "url('img/pdx9.jpg')";
		dragBox.style.backgroundSize = "100% 100%"
	});
})
